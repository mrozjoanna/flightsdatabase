insert into Flight(id, destination) values (11, 'NewYork');
insert into Flight(id, destination) values (12, 'Miami');
insert into Flight(id, destination) values (13, 'Budapest');
insert into Flight(id, destination) values (14, 'Island');
insert into Flight(id, destination) values (15, 'Washington');

insert into Passenger (id, name, surname) values (21, 'Tomasz','Nowak');
insert into Passenger (id, name, surname) values (22, 'Brian','Ione');
insert into Passenger (id, name, surname) values (23, 'Katarzyna','Kowalska');
insert into Passenger (id, name, surname) values (24, 'Jessica','Smith');
insert into Passenger (id, name, surname) values (25, 'Sophia','Jones');

insert into Ticket (id, price, place_class, place_number) values (31, '800 zł', 'economy', '1A');
insert into Ticket (id, price, place_class, place_number) values (32, '400 euro', 'economy', '2A');
insert into Ticket (id, price, place_class, place_number) values (33, '1300 zł', 'economy', '3A');
insert into Ticket (id, price, place_class, place_number) values (34, '2500 zł', 'business', '4A');
insert into Ticket (id, price, place_class, place_number) values (35, '1000 euro', 'business', '5A');

insert into Luggage (id, type, weight) values (41, 'carry-on', '10 kg');
insert into Luggage (id, type, weight) values (42, 'checked-in', '20 kg');
insert into Luggage (id, type, weight) values (43, 'carry-on', '10 kg');
insert into Luggage (id, type, weight) values (44, 'carry-on', '10 kg');
insert into Luggage (id, type, weight) values (45, 'checked-in', '23 kg');

