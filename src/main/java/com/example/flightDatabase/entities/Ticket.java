package com.example.flightDatabase.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Ticket {
    @Id
    @GeneratedValue
    private Long id;
    private String price;
    private String place_class;
    private String place_number;

    @OneToOne(mappedBy = "ticket")
    private Passenger passenger;

    protected Ticket() {
    }

    public Ticket(String price, String place_class, String place_number) {
        this.price = price;
        this.place_class = place_class;
        this.place_number = place_number;
    }

    public Long getId() {
        return id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPlace_class() {
        return place_class;
    }

    public void setPlace_class(String placeClass) {
        this.place_class = placeClass;
    }

    public String getPlace_number() {
        return place_number;
    }

    public void setPlace_number(String place_number) {
        this.place_number = place_number;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", price='" + price + '\'' +
                ", placeClass='" + place_class + '\'' +
                ", place_number='" + place_number + '\'' +
                '}';
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
}
