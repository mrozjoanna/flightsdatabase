package com.example.flightDatabase.entities;

import javax.persistence.*;

@Entity
public class Luggage {
    @Id
    @GeneratedValue
    private Long id;
    private String type;
    private String weight;

    @ManyToOne
    private Passenger passenger;

    protected Luggage() {
    }

    public Luggage(String type, String weight) {
        this.type = type;
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Luggage{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", weight='" + weight + '\'' +
                '}';
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
}
