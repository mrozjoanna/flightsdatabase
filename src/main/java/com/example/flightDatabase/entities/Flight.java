package com.example.flightDatabase.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Flight {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "destination", nullable = false)
    private String destination;

    @ManyToMany(mappedBy = "flights")
    private List<Passenger> passengers = new ArrayList<>();

    protected Flight() {
    }

    public Flight(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void addPassengers(Passenger passenger) {
        passengers.add(passenger);
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", destination='" + destination + '\'' +
                '}';
    }
}
