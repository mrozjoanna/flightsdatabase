package com.example.flightDatabase.repositories;

import com.example.flightDatabase.entities.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class TicketRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Ticket findById(long id) {
        return em.find(Ticket.class, id);
    }

    public void deleteById(long id) {
        Ticket ticketToDelete = findById(id);
        em.remove(ticketToDelete);
    }

    public Ticket save(Ticket ticket) {
        if (ticket.getId() == null) {
            em.persist(ticket);
        } else {
            em.merge(ticket);
        }
        return ticket;
    }
}
