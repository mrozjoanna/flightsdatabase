package com.example.flightDatabase.repositories;

import com.example.flightDatabase.entities.Flight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class FlightRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Flight findById(long id) {
        return em.find(Flight.class, id);
    }

    public void deleteById(long id) {
        Flight flightToDelete = findById(id);
        em.remove(flightToDelete);
    }

    public Flight save(Flight flight) {
        if (flight.getId() == null) {
            em.persist(flight);
        } else {
            em.merge(flight);
        }
        return flight;
    }
}