package com.example.flightDatabase.repositories;

import com.example.flightDatabase.entities.Luggage;
import com.example.flightDatabase.entities.Passenger;
import com.example.flightDatabase.entities.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class LuggageRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Luggage findById(long id) {
        return em.find(Luggage.class, id);
    }

    public void deleteById(long id) {
        Luggage luggageToDelete = findById(id);
        em.remove(luggageToDelete);
    }

    public Luggage save(Luggage luggage) {
        if (luggage.getId() == null) {
            em.persist(luggage);
        } else {
            em.merge(luggage);
        }
        return luggage;
    }

    public void addLuggageWithPassenger(Long passengerId, List<Luggage> luggageList) {
        Passenger passenger = em.find(Passenger.class, passengerId);
        for (Luggage luggage : luggageList) {
            passenger.addLuggageList(luggage);

            luggage.setPassenger(passenger);

            em.persist(luggage);
        }
    }
}
