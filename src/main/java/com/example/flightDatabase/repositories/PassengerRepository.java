package com.example.flightDatabase.repositories;

import com.example.flightDatabase.entities.Flight;
import com.example.flightDatabase.entities.Passenger;
import com.example.flightDatabase.entities.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class PassengerRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Passenger findById(long id) {
        return em.find(Passenger.class, id);
    }

    public void deleteById(long id) {
        Passenger passengerToDelete = findById(id);
        em.remove(passengerToDelete);
    }

    public Passenger save(Passenger passenger) {
        if (passenger.getId() == null) {
            em.persist(passenger);
        } else {
            em.merge(passenger);
        }
        return passenger;
    }

    public void savePassengerWithTicket() {
        Ticket ticket = new Ticket("400 euro", "economy", "20B");
        em.persist(ticket);

        Passenger passenger = new Passenger("Agnieszka", "Mołek");
        passenger.setTicket(ticket);

        em.persist(passenger);
    }

    public void saveManyPassengersWithManyFlights() {
        Passenger passenger = new Passenger("Tom", "Cruise");
        Passenger passenger2 = new Passenger("Ben", "Gruise");
        Passenger passenger3 = new Passenger("Kate", "Born");
        em.persist(passenger);
        em.persist(passenger2);
        em.persist(passenger3);

        Flight flight = new Flight("Maledives");
        em.persist(flight);

        flight.addPassengers(passenger);
        flight.addPassengers(passenger2);
        flight.addPassengers(passenger3);

        passenger.addFlight(flight);
        passenger2.addFlight(flight);
        passenger3.addFlight(flight);

        em.merge(passenger);
        em.merge(passenger2);
        em.merge(passenger3);
    }

    public void savePassengerWithFlight(Passenger passenger, Flight flight) {
        em.persist(passenger);
        em.persist(flight);

        passenger.addFlight(flight);
        flight.addPassengers(passenger);

        em.merge(flight);
        em.merge(passenger);
    }
}