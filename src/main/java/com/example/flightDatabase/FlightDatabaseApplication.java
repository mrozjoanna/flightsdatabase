package com.example.flightDatabase;

import com.example.flightDatabase.entities.Flight;
import com.example.flightDatabase.entities.Luggage;
import com.example.flightDatabase.entities.Passenger;
import com.example.flightDatabase.repositories.FlightRepository;
import com.example.flightDatabase.repositories.LuggageRepository;
import com.example.flightDatabase.repositories.PassengerRepository;
import com.example.flightDatabase.repositories.TicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class FlightDatabaseApplication implements CommandLineRunner {

    @Autowired
    FlightRepository flightRepository;

    @Autowired
    LuggageRepository luggageRepository;

    @Autowired
    PassengerRepository passengerRepository;

    @Autowired
    TicketRepository ticketRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(FlightDatabaseApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        passengerRepository.savePassengerWithTicket();
//
//        ArrayList<Luggage> luggageList = new ArrayList<>();
//        luggageList.add(new Luggage("carry-on", "7 kg"));
//        luggageRepository.addLuggageWithPassenger(25L, luggageList);
//
//        passengerRepository.saveManyPassengersWithManyFlights();

        Passenger passenger = new Passenger("John", "Watson");
        Flight flight = new Flight("Tailand");
        passengerRepository.savePassengerWithFlight(passenger, flight);
    }
}