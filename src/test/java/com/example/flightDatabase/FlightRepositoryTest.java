package com.example.flightDatabase;

import com.example.flightDatabase.entities.Flight;
import com.example.flightDatabase.repositories.FlightRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightDatabaseApplication.class)
public class FlightRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    FlightRepository flightRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById() {
        Flight flight = flightRepository.findById(11L);
        Assert.assertEquals("NewYork", flight.getDestination());
    }

    @Test
    @DirtiesContext
    public void deleteById() {
        flightRepository.deleteById(12L);
        Assert.assertNull(flightRepository.findById(12L));
    }

    @Test
    @DirtiesContext
    public void save_edit() {
        Flight flight = flightRepository.findById(13L);
        Assert.assertEquals("Budapest", flight.getDestination());

        flight.setDestination("London");
        flightRepository.save(flight);

        Flight flight1 = flightRepository.findById(13L);
        Assert.assertEquals("London", flight.getDestination());
    }

    @Test
    @DirtiesContext
    public void save_insert() {
        Flight flight = flightRepository.findById(1L);
        Assert.assertNull(flight);

        Flight flightToAdd = new Flight("Paris");
        flightRepository.save(flightToAdd);

        Flight flight1 = flightRepository.findById(1L);
        Assert.assertEquals("Paris", flight1.getDestination());
    }
}