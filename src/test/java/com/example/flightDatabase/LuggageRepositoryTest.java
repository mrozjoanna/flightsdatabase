package com.example.flightDatabase;

import com.example.flightDatabase.entities.Luggage;
import com.example.flightDatabase.repositories.LuggageRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightDatabaseApplication.class)
public class LuggageRepositoryTest {
    @Autowired
    EntityManager em;
    @Autowired
    LuggageRepository luggageRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById() {
        Luggage luggage = luggageRepository.findById(41L);
        Assert.assertEquals("carry-on", luggage.getType());
    }

    @Test
    @DirtiesContext
    public void deleteById() {
        luggageRepository.deleteById(42L);
        Assert.assertNull(luggageRepository.findById(42L));
    }

    @Test
    @DirtiesContext
    public void save_edit() {
        Luggage luggage = luggageRepository.findById(43L);
        Assert.assertEquals("carry-on", luggage.getType());

        luggage.setType("checked-in");
        luggageRepository.save(luggage);

        Luggage luggage1 = luggageRepository.findById(43L);
        Assert.assertEquals("checked-in", luggage1.getType());
    }

    @Test
    @DirtiesContext
    public void save_insert() {
        Luggage luggage = luggageRepository.findById(1L);
        Assert.assertNull(luggage);

        Luggage luggageToAdd = new Luggage("carry-on", "9 kg");
        luggageRepository.save(luggageToAdd);

        Luggage luggage1 = luggageRepository.findById(1L);
        Assert.assertEquals("carry-on", luggage1.getType());
    }
}