package com.example.flightDatabase;

import com.example.flightDatabase.entities.Luggage;
import com.example.flightDatabase.entities.Passenger;
import com.example.flightDatabase.repositories.PassengerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightDatabaseApplication.class)
public class PassengerRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    PassengerRepository passengerRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById() {
        Passenger passenger = passengerRepository.findById(21L);
        Assert.assertEquals("Tomasz", passenger.getName());
    }

    @Test
    @DirtiesContext
    public void deleteById() {
        passengerRepository.deleteById(22L);
        Assert.assertNull(passengerRepository.findById(22L));
    }

    @Test
    @DirtiesContext
    public void save_edit() {
        Passenger passenger = passengerRepository.findById(23L);
        Assert.assertEquals("Katarzyna", passenger.getName());

        passenger.setName("Aleksandra");
        passengerRepository.save(passenger);

        Passenger passenger1 = passengerRepository.findById(23L);
        Assert.assertEquals("Aleksandra", passenger1.getName());
    }

    @Test
    @DirtiesContext
    public void save_insert() {
        Passenger passenger = passengerRepository.findById(1L);
        Assert.assertNull(passenger);

        Passenger passengerToAdd = new Passenger("Mark", "Jar");
        passengerRepository.save(passengerToAdd);

        Passenger passenger1 = passengerRepository.findById(1L);
        Assert.assertEquals("Mark", passenger1.getName());
    }

    @Test
    @Transactional
    public void getPassengerViaLuggage() {
        Luggage luggage = em.find(Luggage.class, 41L);
        logger.info("LUGGAGE -> {}", luggage);
        logger.info("PASSENGER -> {}", luggage.getPassenger());
    }
}
