package com.example.flightDatabase;

import com.example.flightDatabase.entities.Passenger;
import com.example.flightDatabase.entities.Ticket;
import com.example.flightDatabase.repositories.TicketRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightDatabaseApplication.class)
public class TicketRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    TicketRepository ticketRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById() {
        Ticket ticket = ticketRepository.findById(31L);
        Assert.assertEquals("800 zł", ticket.getPrice());
    }

    @Test
    public void deleteById() {
        ticketRepository.deleteById(32L);
        Assert.assertNull(ticketRepository.findById(32L));
    }

    @Test
    public void save_edit() {
        Ticket ticket = ticketRepository.findById(33L);
        Assert.assertEquals("1300 zł", ticket.getPrice());

        ticket.setPrice("1100 zł");
        ticketRepository.save(ticket);

        Ticket ticket1 = ticketRepository.findById(33L);
        Assert.assertEquals("1100 zł", ticket1.getPrice());
    }

    @Test
    public void save_insert() {
        Assert.assertNull(ticketRepository.findById(1L));

        Ticket ticketToAdd = new Ticket("3000 euro", "business", "4C");
        ticketRepository.save(ticketToAdd);

        Assert.assertEquals("3000 euro", ticketRepository.findById(1L).getPrice());
    }

    @Test
    public void getPassengerViaTicketTest() {
        Ticket ticket = em.find(Ticket.class, 35L);
        logger.info("TICKET -> {}", ticket);
        logger.info("PASSENGER -> {}", ticket.getPassenger());
    }

    @Test
    public void getPassengerWithFlightsTest() {
        Passenger passenger = em.find(Passenger.class, 21L);
        logger.info("PASSENGER -> {}", passenger);
        logger.info("FLIGHTS -> {}", passenger.getFlights());
    }
}
